## README Travel IA Front

Pour récupérer le projet en local, faire :
```
git clone git@gitlab.com:campus26/dwwm4/sahuc-pierre/symfony-api/symfony-api-front.git
```

Ensuite on récupère toutes les dépendances et bundle associés avec l'instruction :
```
npm install 
```

Le lancement du projet en local se fait avec l'instruction :
```
npm run dev
```

Pour modifier la route qui permet la communication entre front et back, on utilisera le fichier .env dans lequel est déclaré la variable VITE_REACT_APP_DOMAIN. Voici sa valeur par défaut :
```
VITE_REACT_APP_DOMAIN=http://127.0.0.1:8000
```

