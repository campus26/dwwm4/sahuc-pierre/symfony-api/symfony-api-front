import polyline from '@mapbox/polyline';


// CALCULE un itinéraire à partir d'une adresse de depart et d'une adresse d'arrivee et en fonction d'un mode de transport
export async function calculRoute(depart, arrivee, transportMode, language) {

  const routeQuerypost = `https://api.openrouteservice.org/v2/directions/${transportMode}`

  console.log('routeQuery', routeQuerypost);

  try {

    const options = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Authorization": "5b3ce3597851110001cf6248c512d188232a4078802a90c872371225"
      },
      body: JSON.stringify({
        "coordinates": [[depart.lng, depart.lat], [arrivee.lng, arrivee.lat]],
        "language": language
      })
    }

    const responsepost = await fetch(routeQuerypost, options);

    let data = await responsepost.json();

    data = extractData(data);
    return data;

  } catch (error) {
    console.error(error.message);
  }
};

// RETOURNE, à partir d'une string, des coordonnées géographiques sous forme d'objet {lat, lng}
export async function getCoordinates(address) {
  if (!address) {
    return;
  }
  const addressQuery = `https://api.openrouteservice.org/geocode/search?api_key=5b3ce3597851110001cf6248c512d188232a4078802a90c872371225&text=${address}`;
  try {
    const response = await fetch(addressQuery);

    const data = await response.json();

    if (data.features.length > 0) {
      const { lat, lng } = { lat: data.features[0].geometry.coordinates[1], lng: data.features[0].geometry.coordinates[0] };
      return ({ lat, lng });
    }
  } catch (error) {
    console.error(error.message);
  }
};

// EXTRAIT des coordoonées, adresses et duree d'une requête
function extractData(data) {
  console.log(data);
  if (data && data.routes[0]) {
    const coords = polyline.decode(data.routes[0].geometry);
    console.log('extrat data', coords);
    const routepoints = coords.map(coord => [coord[0], coord[1]]);
    const distance = data.routes[0].summary.distance;
    const duree = data.routes[0].summary.duration / 3600;
    const steps = data.routes[0].segments[0].steps;
    console.log(steps);
    return { routepoints, distance, duree, steps };
  }
  return [];
}


export async function GetTravelInfoFromAPI(depart, arrivee, transport, villes_stop, inputFilter) {

  const routeQuerypost = `${import.meta.env.VITE_REACT_APP_DOMAIN}/api-openai`;
  // const routeQuerypost = `https://travel-ia-back.pierre-sahuc.com/api-openai`;

  const options = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    mode: 'cors',
    body: JSON.stringify({
      depart: depart,
      arrivee: arrivee,
      transport: transport,
      nb_hebergement: 2,
      type_hebergement: inputFilter.type_hebergement,
      budget_max_hebergement: inputFilter.budget_max,
      nb_activites: 2,
      type_activites: inputFilter.type_activites,
      villes_intermediaires: villes_stop,
      restauration: inputFilter.restauration
    })
  }

  console.log('routeQuery', routeQuerypost);
  console.log('options', options);

  try {

    const responsepost = await fetch(routeQuerypost, options);

    let data = await responsepost.json();

    console.log('Data renvoyées par le back : ', data);

    return data;

  } catch (error) {
    console.error(error.message);
  }
};