import React, { useEffect, useState } from 'react';
import { MapContainer, TileLayer, Marker, Popup, Polyline } from 'react-leaflet';
import L from 'leaflet';
import { initializeSidebar, renderActivityPanel, renderRestPanel } from './Sidebar';
import { decode } from '@mapbox/polyline';

export function Map(props) {
  const InitBounds = [[45.055226997157355, 3.8671530377519145], [45.03012204003403, 3.913172406929098]];
  const [clickedTripData, setClickedTripData] = useState(null);

  useEffect(() => {
    if (props.mapRef.current) {
      props.mapRef.current.eachLayer(layer => {
        if (layer instanceof L.Marker || layer instanceof L.Polyline) {
          props.mapRef.current.removeLayer(layer);
        }
      });

      const stages = props.DataTrip.etapes;
      let polylines = [];

      if (stages) {
        for (let prop in stages) {
          if (stages[prop]) {
            const tripInfo = stages[prop];
            const decodedPolyline = decode(tripInfo.polyline.routes[0].geometry);
            const coordsEtape = [Number(tripInfo.lat), Number(tripInfo.lon)];
            const marker = L.marker(coordsEtape).addTo(props.mapRef.current);
            marker.tripData = tripInfo;
            marker.bindPopup(tripInfo.ville_etape);
            marker.on('click', (e) => {
              const clickedTripData = e.target.tripData;
              console.log(clickedTripData);
              setClickedTripData(clickedTripData);
              if (sidebar) {
                sidebar.open('activity', clickedTripData);
              }
            });

            marker.addTo(props.mapRef.current);
            let currentPolyline = L.polyline(decodedPolyline, { color: 'blue' }).addTo(props.mapRef.current);
            polylines.push(currentPolyline);
          }
        }
      }

      const coordsDepart = [Number(props.DataTrip.ville_depart.lat), Number(props.DataTrip.ville_depart.lon)];
      const dep = L.marker(coordsDepart).bindPopup(props.DataTrip.ville_depart.nom).addTo(props.mapRef.current);

      //creation d'un objet bounds vide
      let bounds = L.latLngBounds([]);

      // on parcourt le tableau contenant les polyline des etapes pour étendre les bounds
      polylines.forEach(function (polyline) {
        bounds.extend(polyline.getBounds());
      });

      props.mapRef.current.fitBounds(bounds);

      const sidebar = initializeSidebar(props.mapRef.current, props.DataTravel, clickedTripData, () => renderActivityPanel(clickedTripData), clickedTripData, () => renderRestPanel(clickedTripData));

      return () => {
        sidebar.remove();
      };
    }
  }, [props.DataTrip, props.mapRef, clickedTripData]);

  return (
    <div>
      <MapContainer bounds={InitBounds} zoom={13} ref={props.mapRef}>
        <TileLayer
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
      </MapContainer>
    </div>
  );
}