import L from 'leaflet';
import 'leaflet-sidebar-v2/css/leaflet-sidebar.css';
import 'leaflet-sidebar-v2/js/leaflet-sidebar.js';
import './Sidebar.css';
import ActivityIcon from '../../assets/activity.png';
import EatIcon from '../../assets/eat.png';
import SleepIcon from '../../assets/sleep.png';
import InfoIcon from '../../assets/info.png';


export function renderActivityPanel(clickedTripData) {
  if (!clickedTripData || !clickedTripData.activites || clickedTripData.activites.length === 0) return "<div>Aucune activité disponible pour cette étape.</div>";

  let activitiesHtml = "<div><h3>Activités proposées</h3><ul>";
  clickedTripData.activites.forEach(activity => {
      activitiesHtml += `
          <li>
              <h4>${activity.nom}</h4>
              <p>${activity.description}</p>
          </li>
      `;
  });
  activitiesHtml += "</ul></div>";

  return activitiesHtml;
}

export function renderRestPanel(clickedTripData) {
  if (!clickedTripData || !clickedTripData.hebergement || clickedTripData.hebergement.length === 0) return "<div>Aucune hebergement disponible pour cette étape.</div>";

  let activitiesHtml = "<div><h3>Hebergements proposées</h3><ul>";
  clickedTripData.hebergement.forEach(rest => {
      activitiesHtml += `
          <li>
              <h4>${rest.nom}</h4>
              <p>${rest.prix}</p>
              <p>${rest.adresse}</p>
          </li>
      `;
  });
  activitiesHtml += "</ul></div>";

  return activitiesHtml;
}

function renderDataResults(dataTravel) {
  if (!dataTravel) return "";

  let resultHtml = "";
  if (dataTravel.distance)
    resultHtml += `<p> Distance : ${formatDistance(dataTravel.distance)}</p>`;
  if (dataTravel.duree)
    resultHtml += `<p> Durée : ${formatDuration(dataTravel.duree, "minutes")}</p>`;
  if (dataTravel.instructions) {
    resultHtml += "<div><h2>Instructions</h2><ul>";
    dataTravel.instructions.forEach(instruction => {
      if (instruction.name !== "-") {
        resultHtml += `
          <li>
            <div>
              <h3>${instruction.name}</h3>
              <i>${instruction.instruction}</i>
            </div>
            <div>
              <p>${formatDistance(instruction.distance)}</p>
              <p>${formatDuration(instruction.duration, "seconds")}</p>
            </div>
          </li>
        `;
      }
    });
    resultHtml += "</ul></div>";
  }

  return resultHtml;
}

function formatDuration(duration, unitSource) {
  if (unitSource === "seconds") {
    const hours = Math.floor(duration / 3600);
    const minutes = Math.floor((duration - (hours * 3600)) / 60);
    const formatHeureMinute = (hours < 10 ? "0" : "") + hours.toString() + "h " + (minutes < 10 ? "0" : "") + minutes.toString() + "min";
    return formatHeureMinute;
  } else if (unitSource === "minutes") {
    const hours = Math.floor(duration);
    let minutes = (duration - hours)*60;
    minutes = minutes.toFixed(0);
    const formatHeureMinute = (hours < 10 ? "0" : "") + hours.toString() + "h " + (minutes < 10 ? "0" : "") + minutes.toString() + "min";
    return formatHeureMinute;
  }
  return "";
}

function formatDistance(distance) {
  const km = distance/1000;
  return km.toFixed(2) + " km";
}

export function initializeSidebar(mapRef, dataTravel, clickedTripData, renderActivityPanel) {
  var sidebar = L.control.sidebar({
    autopan: true,
    closeButton: true,
    container: 'sidebar',
    position: 'right',
  }).addTo(mapRef);

  const panelActivity = {
    id: 'activity',
    tab: `<img src=${ActivityIcon} class="fa fa-gear" width="100%">`,

    // /src/assets/activity.png" class="fa fa-gear" width="100%">',
    pane:  `<div>
      ${renderActivityPanel(clickedTripData)}
    </div>`,
    title: "Suggestions d'activité",
    position: 'top',
  };
  sidebar.removePanel('activity');
  sidebar.addPanel(panelActivity);

  var panelEat = {
    id: 'eat',
    tab: `<img src=${EatIcon} class="fa fa-gear" width="90%">`,
    pane: '<div>Ceci est le contenu</div>',
    title: "Suggestions de lieux de restauration",
    position: 'top',
  };
  sidebar.addPanel(panelEat);

  var panelRest = {
    id: 'rest',
    tab: `<img src=${SleepIcon} class="fa fa-gear" width="90%">`,
    pane: `<div>
    ${renderRestPanel(clickedTripData)}
  </div>`,
    title: "Offres d'hébergement",
    position: 'top',
  };
  sidebar.addPanel(panelRest);

  var panelInfo = {
    id: 'info',
    tab: `<img src=${InfoIcon} class="fa fa-gear" height="80%" width="85%">`,
    pane: `
      <div>
        <h3>Résultats de l'itinéraire</h3>
        ${renderDataResults(dataTravel)}
      </div>
    `,
    title: "Informations générales",
    position: 'top',
  };
  sidebar.addPanel(panelInfo);

  return sidebar;
}