import React, { useState, useEffect } from 'react';
import { GetTravelInfoFromAPI } from "../../GeoTools";
import './Menu.css';
import ArretInput from "./ArretInput";
import Filter from "./Filter";

export function Menu(props) {
  const [isBlockVisible, setIsBlockVisible] = useState(false);
  const [arret, setArret] = useState('');
  const [arrets, setArrets] = useState([]);

  const [inputFilter, setInputFilter] = useState({
      nb_herbergements: 2,
      type_hebergement: "hotel",
      budget_max: 100,
      nb_activites: 2,
      type_activites: "culture",
      restauration: false
  });

  const handleLinkClick = () => {
    setIsBlockVisible(!isBlockVisible);
  }

  const addArret = async (newArret) => {
    setArrets([...arrets, newArret]);
  };

  useEffect(() => {
        if (arrets.length > 0) {
            setArret(arrets[0]);
        } else {
            setArret('');
        }
    }, [arrets]);


  const deleteArret = (index) => {
    const newArrets = [...arrets];
    newArrets.splice(index, 1);
    setArrets(newArrets);
  };

  const handleInputAdressChange = async (e, setter) => {
    const input = e.target.value;
    setter(input);
  };

  const handleDataTripChange = async () => {
    const villes_stop = String(arrets.map(arret => arret.name));

    const data = await GetTravelInfoFromAPI(props.InputDepart, props.InputArrivee, props.transport, villes_stop, inputFilter);
    props.setDataTrip(data);
   
    return data;
  };

  useEffect(() => {
    console.log('props.dataTrip apres handleDataTripChange', props.DataTrip);
    
    // // reinitialisation des données du formulaire
    setInputFilter({
      nb_herbergements: 2,
      type_hebergement: "hotel",
      budget_max: 100,
      nb_activites: 2,
      type_activites: "sport",
      restauration: false
    });

  }, [props.DataTrip]);

  return (
    <div className="menu">
      <div className="form">
        <h1>Mon itinéraire</h1>
        <fieldset>
          <legend>Mode de transport</legend>
          <input type="radio" id="driving-car" name="transport" value="driving-car" onChange={props.handleTransportChange} checked={props.transport === "driving-car"} />
          <label htmlFor="driving-car">Voiture</label>
          <input type="radio" id="cycling-regular" name="transport" value="cycling-regular" onChange={props.handleTransportChange} checked={props.transport === "cycling-regular"} />
          <label htmlFor="cycling-regular">Vélo</label>
          <input type="radio" id="foot-walking" name="transport" value="foot-walking" onChange={props.handleTransportChange} checked={props.transport === "foot-walking"} />
          <label htmlFor="foot-walking">Marche</label>
          <input type="radio" id="wheelchair" name="transport" value="wheelchair" onChange={props.handleTransportChange} checked={props.transport === "wheelchair"} />
          <label htmlFor="wheelchair">Chaise roulante</label>
        </fieldset>
        <h3>Sélectionnez le point de départ</h3>
        <input className="inputRoute" type="text" placeholder="Départ" onChange={(e) => { handleInputAdressChange(e, props.setInputDepart) }} value={props.InputDepart} />
        <h3>Sélectionnez votre point d'arrivée</h3>
        <input className="inputRoute" type="text" placeholder="Arrivée" onChange={(e) => { handleInputAdressChange(e, props.setInputArrivee) }} value={props.InputArrivee} />
        <button className="stop-button" onClick={handleLinkClick}>Souhaitez-vous ajouter des points d'arrêt supplémentaires?</button>
        {isBlockVisible && 
          <ArretInput
          isBlockVisible={isBlockVisible}
          arrets={arrets}
          addArret={addArret}
          setArrets={setArrets}
          deleteArret={deleteArret}
          />
        }
        <Filter
          inputFilter={inputFilter}
          setInputFilter={setInputFilter}
        />
        <button type="button" className="button-calculer" onClick={() =>{handleDataTripChange()}}>Calculer</button>
        {/* <button type="button" onClick={() =>{handleDataTripChange()}}>TestAPI</button> */}
      </div>
    </div>
  );
}