import React, { useState } from 'react';
import { getCoordinates } from '../../GeoTools';
import removeIcon from '../../assets/remove.png';

const ArretInput = ({ isBlockVisible, arrets, addArret, deleteArret }) => {
    const [inputValue, setInputValue] = useState('');

    const handleAjoutezClick = async () => {
        if (inputValue.trim() !== '') {
          const arretCoordinates = await getCoordinates(inputValue);
          if (arretCoordinates) {
            addArret({ name: inputValue, coordinates: arretCoordinates });
            setInputValue('');
          } else {
            console.error('error');
          }
        }
      };
    
      return (
        <div className={isBlockVisible ? 'block-arrets' : 'hidden'}>
          <h4>Ajoutez des arrêts supplémentaires</h4>
          <div className="input-wrapper">
            <input type="text" value={inputValue} onChange={(e) => setInputValue(e.target.value)} />
            <button className="ajoutez" onClick={handleAjoutezClick}>Ajoutez</button>
          </div>
          {arrets.length > 0 ? (
            <ul className="arret-list">
              {arrets.map((arret, index) => (
                <div className={`arret-${index + 1}`} key={index}>
                  <li>
                    {arret.name}
                    <button className="delete-button" onClick={() => deleteArret(index)}><img src={removeIcon} className="icon-remove" alt="remove" /></button>
                  </li>
                </div>
              ))}
            </ul>
          ) : (
            <div className="empty">
              <p>Vous n'avez pas d'arrêt supplémentaire</p>
            </div>
          )}
        </div>
      );
    };
    
    export default ArretInput;