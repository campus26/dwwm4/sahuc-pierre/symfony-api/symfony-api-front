import './Filter.css';
const Filter = (props) => {

    const handleInputFilterChange = (e, type) => {    
        switch (type) {
          case "type_hebergement": {
            props.setInputFilter(prevState => ({
              ...prevState,
              type_hebergement: e.target.value
            }))
            break;
          }
          case "budget_max": {
            props.setInputFilter(prevState => ({
              ...prevState,
              budget_max: e.target.value
            }))
            break;
          }
          case "type_activites": {
            props.setInputFilter(prevState => ({
              ...prevState,
              type_activites: e.target.value
            }))
            break;
          }
          case "restauration": {
            props.setInputFilter(prevState => ({
              ...prevState,
              restauration: e.target.checked
            }))
            break;
          }
        }
      }

    return (
    <div className="filter">
        <fieldset className="select-activity">
            <legend>Type d'activité</legend>
                <input type="radio" id="sport" name="activity" value="sport" onChange={(e) => {handleInputFilterChange(e, 'type_activites');}} checked={props.inputFilter.type_activites === "sport"} />
                <label htmlFor="sport">Sport</label>
                <input type="radio" id="culture" name="activity" value="culture" onChange={(e) => {handleInputFilterChange(e, 'type_activites');}} checked={props.inputFilter.type_activites === "culture"}/>
                <label htmlFor="culture">Culture</label>
                <input type="radio" id="insolite" name="activity" value="insolite" onChange={(e) => {handleInputFilterChange(e, 'type_activites');}} checked={props.inputFilter.type_activites === "insolite"}/>
                <label htmlFor="insolite">Insolite</label>  
                <input type="radio" id="touristique" name="activity" value="touristique" onChange={(e) => {handleInputFilterChange(e, 'type_activites');}} checked={props.inputFilter.type_activites === "touristique"}/>
                <label htmlFor="touristique">Touristique</label>
        </fieldset>
        <fieldset className="select-hebergement">
            <legend>Type d'hebergement</legend>
                <input type="radio" id="hotel" name="hebergement" value="hotel" onChange={(e) => {handleInputFilterChange(e, 'type_hebergement');}} checked={props.inputFilter.type_hebergement === "hotel"}/>
                <label htmlFor="hotel">Hotel</label>
                <input type="radio" id="camping" name="hebergement" value="camping" onChange={(e) => {handleInputFilterChange(e, 'type_hebergement');}} checked={props.inputFilter.type_hebergement === "camping"}/>
                <label htmlFor="camping">Camping</label>
                <input type="radio" id="chambre" name="hebergement" value="chambre" onChange={(e) => {handleInputFilterChange(e, 'type_hebergement');}} checked={props.inputFilter.type_hebergement === "chambre"}/>
                <label htmlFor="chambre">Chambre</label>
        </fieldset>
        <div className="select-budget">
            <h4>Budget pour l'hébergement</h4>
            <select onChange={(e) => {handleInputFilterChange(e, 'budget_max');}}>
	            <option value='50'>50 euros</option>
	            <option value='100'>51-100 euros</option>
	            <option value='150'>101-150 euros</option>
            </select>
        </div>
        <form className="place-for-eat">
            <label htmlFor="restauration"><h4>Suggestions de lieux de restauration</h4></label>
            <input type="checkbox" id="restauration" name="restauration" onChange={(e) => {handleInputFilterChange(e, 'restauration');}} checked={props.inputFilter.restauration}/>
        </form>
    </div>
    );
};

export default Filter;