import { useState, useEffect, useRef } from 'react'
import './App.css'
import { Menu } from './components/menu/Menu'
import { Map } from './components/map/Map';
import { getCoordinates, calculRoute } from './GeoTools';

export function App() {

  // passer InputDepart, InputArrive et transport au composant Menu + ajouter des propriétés supplémentaires à DataTravel qui seraient mises à jour par ces hooks
  const [InputDepart, setInputDepart] = useState("Paris");
  const [InputArrivee, setInputArrivee] = useState("Orléans");
  const [transport, setTransport] = useState("cycling-regular");
  // const [DataTravel, setDataTravel] = useState({
  //   routePoints: [],
  //   instructions: [],
  //   distance: "",
  //   duree: "",
  //   latDepart: "",
  //   lngDepart: "",
  //   latArrivee: "",
  //   lngArrivee: "",
  // });

  const [DataTrip, setDataTrip] = useState({});


  const mapRef = useRef();

  function handleTransportChange(e) {
    setTransport(e.target.value);
  }

  // async function getRouteData() {

  //   const DepartCoords = await getCoordinates(InputDepart);
  //   const ArriveeCoords = await getCoordinates(InputArrivee);
  //   const data = await calculRoute(DepartCoords, ArriveeCoords, transport, "fr-fr");

  //   const DataToReturn = {
  //     routePoints: data.routepoints,
  //     instructions: data.steps,
  //     distance: data.distance,
  //     duree: data.duree,
  //     latDepart: DepartCoords.lat,
  //     lngDepart: DepartCoords.lng,
  //     latArrivee: ArriveeCoords.lat,
  //     lngArrivee: ArriveeCoords.lng,
  //   };

  //   setDataTravel(DataToReturn);

  // }

  return (
    <>
      <Menu
        InputDepart={InputDepart}
        setInputDepart={setInputDepart}
        InputArrivee={InputArrivee}
        setInputArrivee={setInputArrivee}

        transport={transport}
        handleTransportChange={handleTransportChange}

        // DataTravel={DataTravel}
        // getRouteData={getRouteData}

        DataTrip={DataTrip}
        setDataTrip={setDataTrip}
      />

      <Map
        mapRef={mapRef}
        // DataTravel={DataTravel}

        DataTrip={DataTrip}

        // InputDepart={InputDepart}
        // setInputDepart={setInputDepart}
        // InputArrivee={InputArrivee}
        // setInputArrivee={setInputArrivee}
      />
    </>
  )
}